<?php

class seculoAno {
    
    //Função que retorna o século
    public function RetornaSeculo($ano){
        
        //Pega os dois ultimos digitos do ano para verificar qual século é
        $verificaSeculo  = substr($ano, -2);
        
        //Verifica se os dois últimos digitos são iguais a zero, caso não for soma os dois primeiros digitos mais 1.
        if($verificaSeculo == '00'){
            $seculo = substr($ano, 0,2);
        }else {
            $seculo = substr($ano,0, 2) + 1;
        }
        
        //Retorna o século
        return $seculo;
    }
}
