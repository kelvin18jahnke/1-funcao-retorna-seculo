<?php
    require_once './funcoes/seculoAno.php';
    
    $seculoAno = new seculoAno();
    
    if (isset($_POST['ano'])) {
        
        $ano =  $_POST['ano'];
        
        if($ano == ''){
            echo '<div class="alert alert-info" role="alert">
                Informe um ano para retornar o século!
                </div>';
            
        }else{
            //Funnção que retorna o século do ano.
            $seculo = $seculoAno->RetornaSeculo($ano);  
        }
    }else{
        $ano = '';
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        <title>Século Ano:</title>
    </head>
    <body>
       
        <form action="index.php" method="POST">
            <div class="row g-3 align-items-center" style="margin: 2% 10%;">
                <h1>Função que Retorna o ano</h1>
                <div class="col-auto">
                    <label  class="col-form-label">Informe o ano:</label>
                </div>
                <div class="col-auto">
                    <input type="number" name="ano" value="<?=$ano?>" class="form-control">
                </div>
                <div class="col-auto">
                    <span  class="form-text">
                       <?php
                        if(isset($seculo) && $seculo != ''){
                            echo 'O seculo é : ' . $seculo;
                        } 
                       ?>
                    </span>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </form>
    </body>
</html>
